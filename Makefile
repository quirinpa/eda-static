EXE := proj

IDIR := include
SDIR := src
ODIR := obj

ODIRS := obj obj/io
VPATH = $(IDIR) $(SDIR)

WARNINGS := -Wall -Wextra -pedantic -Wshadow -Wpointer-arith \
	-Wcast-align -Wwrite-strings -Wmissing-declarations -Winline \
	-Wno-long-long -Wuninitialized -Wconversion -Wredundant-decls -Wdouble-promotion

ifeq (,$(wildcard $(SDIR)/main.c))
	AEXT := c
	EXT := cpp
	CC := $(CXX)
	CFLAGS := -O
else
	EXT := c
	AEXT := cpp
	CFLAGS := -g
	WARNINGS :=	$(WARNINGS) -Wnested-externs -Wstrict-prototypes -Wmissing-prototypes	
	CC := gcc
endif

CFLAGS := $(CFLAGS) -ansi $(WARNINGS) -I$(IDIR)

SRC := $(patsubst $(SDIR)/%, %, $(wildcard $(SDIR)/*.$(EXT) $(SDIR)/*/*.$(EXT)))
OBJ := $(addprefix $(ODIR)/, $(SRC:%.$(EXT)=%.o))

.PHONY: all dist todolist conv tests

SRCF := 
all: $(EXE)

$(EXE): $(OBJ)
	@echo $(CC)  $@
	-@$(CC) -o $@ $^ $(CFLAGS)

# make.mad-scientist.net/papers/advanced-auto-dependency-generation/#norule
$(ODIR)/%.o: %.$(EXT) | $(ODIRS)
	@echo $(CC) $@
	-@$(CC) -c -o $@ $< -MMD $(CFLAGS)

-include $(OBJ:.o=.d)

$(ODIRS):
	-@mkdir -p $@

clean:
	-@$(RM) -r $(wildcard $(ODIR) $(EXE) proj.zip tags)

dist:
	@zip proj.zip $(IDIR)/*.h $(IDIR)/*/*.h $(SRC:%=$(SDIR)/%) Makefile

conv: clean
	@for file in $(SRC:%=$(SDIR)/%); do\
		mv $$file $${file/.$(EXT)/.$(AEXT)}; \
	done;\
	echo changed to $(AEXT) mode

todolist:
	-@for file in $(wildcard $(SRC:%=$(SDIR)/%) $(IDIR)/*/*.h $(IDIR)/*.h); do\
	 fgrep -H -e TODO -e FIXME $$file; done; true

