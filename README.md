Projeto de EDA - Cantina

Paulo André Azevedo Quirino

quirinpa@gmail.com

2014215

# Prefácio
Este README contém muita informação, e não era preciso relatório nesta primeira fase, por isso, se quiser, leia apenas o tópico **compilação**, que é o mais relevante. O código usado neste projeto é completamente da minha autoria, e não permito o seu uso sem me seja dado crédito. Obrigado pela compreensão.

# Índice
- Introdução
- Linguagem (C)
- Compilação
- Malloc
- Design de código / Legibilidade
- Comportamentos
- HashTables VS SortedArrays (fim)

# Introdução
Enfrentei uma série de dificuldades ao tentar fazer um projeto melhor, o facto de estar em Linux e não ter acesso ao Visual Studio, por exemplo, é algo que vai contra as especificações do enunciado, mas os docentes, conscientes da minha situação, foram razoáveis e concordaram comigo que poderia utilizar as ferramentas a meu dispôr. Outro "problema" que surgiu foi ter que adaptar o código a especificidades como **não usar estruturas de dados dinâmicas**, algo que suponho que é requerido para introduzir os meus colegas a programação em **C/C++** sem exigir que saibam tratar dados dinâmicamente, e para nos mostrar as vantagens e desvantagens dessas duas estratégias. Para mim, no início, pareceu ser algo que desmotiva o uso de métodos melhores, porém, no decorrer do desenvolvimento, apercebi-me que fui desafiado a encontrar soluções discutivelmente melhores do que tinha planeado com estruturas dinâmicas. O problema principal foi o de não ter feito o trabalho em grupo, isso deve-se a vários factores, a maioria já foram discutidos com o Professor José Luís. Resta-me a mim fazer o melhor projecto que conseguir, algo que faço com prazer, e a convicção que a justiça permanecerá no decorrer da cadeira, e das avaliações.

# Linguagem (C)
Na disciplina foram dadas duas linguagens de programação, **C** e **C++**. Após experimentar ambas, apercebi-me da diversidade de *Standards* em que podem ser escritas. Visto ser tão complexo saber apenas uma delas bem, e dado o facto de uma ser baseada na outra, resolvi focar-me no **C**, mais especificamente no **Standard ISO C90**, e foi com essa linguagem em mente que desenvolvi o projeto, abedicando de funcionalidades como as **classes** (matéria de *OOP*) e outras particularidades da linguagem.

Sugiro, para exemplificar, que se observe os problemas que se têm com o **cin**, que para serem resolvidos exigem uma compreensão mais detalhada do funcionamento de *Streams*, e dependem do comportamento da consola ou até do sistema operativo, a não ser que sejam tomadas medidas para o evitar. Algo que acontece também com o **getchar**, **scanf**, etc.

A minha solução para receber dados do utilizador é uma função que aloca um array de caracteres e o redimensiona conforme necessário (ou o fará na segunda fase, quando isso for autorizado), devolvendo uma variàvel do tipo Str, que desenvolvi para não precisar de chamar **strlen** para saber o tamanho de "strings", quando é uma operação que sei que vou fazer mais vezes (por exemplo, quando a vou gravar num ficheiro).

Se quiser converter uma **Str** num número, utilizo outra função que desenvolvi - *strToULL*, que providencia tratamento de erros para quando os valores estão fora dos limites das variàveis, ou para quando tem caracteres inválidos. Se quiser ler e converter directamente, chamo *lrULL*, por exemplo:
```c
unsigned char curso = (unsigned char) lrULL("Id do curso", 0, 255, NULL);
```
Em que "Id do curso" é o "pedido ao utizador" ou *prompt*, os dois argumentos que se seguem são, respectivamente, os limites inferior e superior de valores considerados válidos, e o último argumento, que neste caso é NULL, poderá ser um ponteiro onde guardar o primeiro caracter não válido. Para verificar se há erros, incluo a biblioteca **errno.h**, estabelecendo a variàvel errno como *EINVAL* ou *ERANGE* dependendo do tipo de erro, e fazendo comparações sobre esse valor após chamar a função. O strToULL foi *inspirado* no [strtonum](www.tedunangst.com/flak/post/the-design-of-strtonum). Não copiei o código, mas por necessidade, fui adicionando pormenores parecidos com a implementação em questão. Digo isto sem querer descartar a influencia que ler sobre o *strtonum* teve nas minhas decisões.

A insatisfação com soluções superficiais, juntamente com o facto de não termos dado classes (a não ser por abstração), e os outros factores que já referi, fizeram com que me parece mais boa ideia utilizar *C*, mas como o enunciado diz explicitamente que o projecto deveria estar em *C++*, o meu Makefile contém uma instrução para o converter, que é o **"make conv"**. Mais informações no tópico seguinte.

# Compilação
Como desenvolvi o projeto em **Linux**, tenho a responsabilidade de providenciar um método para compilar e executar o projeto em Windows. Assim como de **manter o código compatível**, utilizando **bibliotecas Standard**, e as **capacidades do pré-processador**.

Em Linux, como é "tradicional", utilizei ferramentas separadas para cada tarefa. *Vim* para editar, *GNU Make* como "build tool", e *CC* para compilar, linkar e gerar grafos de depêndencias para incluir no **Makefile**. No Windows, o *Visual Studio* trata destas diferentes tarefas, por isso tentei adaptar o meu *Makefile* para funcionar com o seu compilador, *CL*, e com a sua "build tool",  *NMake*, até fazer **ficheiros de solução**, mas acabei por encontrar soluções melhores.

Das duas soluções que se seguem, a primeira é mais fácil, lenta e menos apropriada, e a segunda requer a alteração da variàvel **PATH**. É essa que recomendo.

## MSYS2
O **MSYS2**, apesar de grande, e de utilizar Cygwin (o que implica que não corre na consola do Windows sem dependências externas), foi a solução mais automática e acessível que encontrei. Se o quiser utilizar, poderá encontrá-lo através deste [link](https://msys2.github.io). Basta fazer download, instalar, abrir a shell do MSYS2 e correr os comandos:
```shell
pacman -S gcc tar
curl -JO https://gitlab.com/quirinpa/eda-static/repository/archive.tar.gz?ref=master
tar -xvf eda-static*
cd eda-static*
make
```

ou numa linha:
```shell
pacman -S gcc tar && curl -JO https://gitlab.com/quirinpa/eda-static/repository/archive.tar.gz?ref=master && tar -xvf eda-static* && cd eda-static* && make
```

## MinGW + MSYS
Apesar de ser mais complexo de instalar, o MinGW é mais rápido, e compila utilizando as bibliotecas de C do windows (em particular, *Borland conio.h*). Pode encontrar o link para o instalador [aqui](https://sourceforge.net/projects/mingw/files/latest/download?source=files). Feito o download, a instalação também não é difícil, só tem que se certificar que, chegando à parte de selecionar o que instalar, escolhe:
- mingw32-base
- mingw32-gcc-g++
- msys-base

Para cada um desses, clique com o botão direito e escolha **Mark for Installation**. Depois vá ao menu **Installation** e clique em **Apply changes**.

Quando acabar a instalação precisa de adicionar os caminhos para os executáveis do MinGW e do msys ao **PATH**.

Pressione (tecla do Windows)+(break/pause), **Definições avançadas do sistema**, **Variàveis de Ambiente**, selecione **path**, clique em **Editar** e adicione **C:\MinGW\bin** e **C:\MinGW\msys\1.0\bin**. Depois faça o download do projeto, por exemplo, em zip, Clicando em **Download zip** na página do repositório. Extraia para uma pasta e aceda à mesma através da linha de comandos. depois execute o comando **make**, e já está.

## Finalmente
Independentemente da forma que escolheu, terá o projeto pronto para executar. Como referi antes, pode converter entre um projeto de C e de C++ com o comando **"make conv"**.

Actualmente, o processo de compilação mostra mensagens de erro referentes ao ficheiro **HashTable.$(EXT)**, que se queixam que os argumentos da função rehash não são utilizados. Isso deve-se ao facto de não se poder redimensionar arrays nesta fase do projeto, e não afecta a execução do programa. Dependendo das opções de compilação (em particular em modo de C++), aparecem outros avisos referentes aos parametros de formatação do sprintf e do printf **%llu** e **%lu**, mas que também não afectam o comportamento do programa.

# Malloc
O **malloc** é utilizado neste projeto, pode-se chamar a isso de "alocação dinâmica", mas os professores disseram que usar malloc seria ok, desde que não realocasse os dados, ou usasse objectos ligados (por outras palavras). De qualquer forma, parece-me merecedor de uma pequena explicação do porquê de ter escolhido usar esta "controversa" ferramenta. A ùnica forma de evitar fazer malloc (ou new) seria se as funções que criam os diversos tipos de dados devolvessem o objecto em si, em vez de um ponteiro para o mesmo. Se tentar devolver o endereço de um objecto que é alocado no âmbito da função, por exemplo:
```c
int *dobro(int a) {
	int r = a*2;
	return &r;
}
```
O espaço em memória onde está alocado o conteúdo de *r* deixaria de estar reservado aquando do terminar da função, e isso poderia levar a resultados imprevisíveis. Ver [este link](www.stackoverflow.com/questions/12380758/c-error-function-returns-address-of-local-variable).

Se devolvesse o objecto em si, estaria constantemente a fazer cópias, o que é desnecessário. Além disso, **não estou a redimensionar arrays**, nem tenho **tipos de dados dinâmicos** como Listas, Pilhas, etc. Só estou a criar objectos quando são necessários, como o outro método faria, e a inseri-los nos meus tipos de dados estáticos. Esses podem fazer aparecer mais um assunto controverso: Estás a usar malloc quando lês o número de alunos do ficheiro! Pois estou. E *int arr[n + 10]*, seria legal?

# Design de código / Legibilidade
Quando se está a aprender, é uma luta constante fazer decisões em relação à organização do código de um projecto. Que tipos de dados usar? Que convenções decidir para nomear os identificadores das variàveis e das funções? Como organizar os ficheiros? É um processo evolutivo, *progressive development*, por assim dizer. Quando o projecto começa a ficar estável, repara-se que algumas funções que declarámos em ficheiros separados só se usam uma vez, e portanto não precisam de estar fora do ficheiro, talvez nem sequer em funções separadas. Eventualmente são feitas decisões, que não listar uma a uma, mas espero terem sido as melhores, na apresentação estarei pronto para as discutir. O código está documentado, mas só parcialmente, visto que foi a última coisa que fiz. Os cabeçalhos referentes aos tipos de dados mais complexos estão documentados, e tentei ter uma boa estrutura em geral. Sugiro que se comece a ver o **main.c**.

# Comportamentos
Existem algumas coisas que não estão implícitas nem explícitas no enunciado (menos coisas do que pensava quando comecei a trabalhar), e outras que ficaram ao nosso critério. Estas decisões afectam o comportamento do programa de maneiras diferentes:

## Datas
As datas são, internamente, **o número de dias que passou desde 1 de Janeiro de 1900** (referido no código como EPOCH). Essa data foi escolhida porque é adequada no âmbito do projecto, e, apesar de o conceito existir noutras àreas, como na astrologia, e de no *time.h*, não copiei código absolutamente nenhum. O esforço a que me submeti para as implementar foi que pudesse obter os seus benefįcios:
- Poder incrementar uma data para ter o dia seguinte
- Permitir que o utilizador possa digitar "hoje" para se referir à data actual, ou "amanha" para se referir ao dia seguinte
- Verificar se o utilizador está a **encomendar uma refeição para o passado**, e impedir esse absurdo
O que nos leva ao próximo tópico.

## Validação do input
Já referi neste README coisas que me ajudam a validar o input. Se é descabido num determinado contexto, tento informar o utilizador com uma mensagem na qual indico qual seria o formato correcto.

## Cores
As cores foram algo desenvolvido na epoca final do projecto, pensei que não iam funcionar em Windows mas quando trabalhei no procedimento de compilação para este, descobri que sim. Ficou uma funcionalidade que apesar de inútil, é bonitinha.

## O programa não consegue redimensionar arrays
Por isso mostra uma mensagem de erro e sai. Conforme requerido.

## Etc

# HashTables VS SortedArrays
Duas estruturas de dados que utilizo no projecto para armazenar ou aceder a alunos, funcionários e refeições. As HashTables são úteis porque têm acessos, insersões e remoções, em média, constantes os piores casos são muito raros, e no caso das minhas nunca chegam a O(n). Os SortedArrays (pesquisa binária) porque uma pesquisa numa HashTable (que não está ligada como uma lista) é lenta. Para cada situação utilizo o que me pareceu mais adequado, não vou entrar em mais detalhe visto que este "Relatório" está a começar a ficar grande. Penso que o código e a documentação poderão preencher outras questões que se tenha, caso contrário, estou sempre disponível para o fazer. Estes tipos de dados permitiam-me implementar quase todos os pontos do enunciado (inclusive os da segunda fase), mas não tive tempo. Dito isto,

Obrigado e bom trabalho.
