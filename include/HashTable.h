/* HashTable.h
 *
 * Aqui está declarado o tipo de dados HT (ou HashTable), e todas
 * as funções relacionadas com o seu uso, de forma generalista.
 *
 * Serve para armazenar ponteiros para qualquer tipo de dados, para
 * providenciar pesquisas rápidas quando não é necessário que estes
 * sejam apresentados ordenadamente.
 *
 * São utilizadas três designações que vale a pena mencionar:
 * Tabela - ponteiro para uma HashTable (ou HT)
 * Objectos - ponteiros para valores do tipo que a tabela guarda
 * Chaves - ponteiros para valores (únicos) que podem ser obtidos
 *  através de uma chamada à função getKey (ver documentação da
 *  estrutura HT) com um objecto como argumento. 
 *
 * Referências:
 * cs.nyu.edu/~lerner/spring11/proj_hopscotch.pdf
 * youtube.com/watch?v=z0lJ2k0sl1g
 * Outras aulas do MIT e vídeos de youtube que lamento
 * não ter apontado, e outros links que estão na implementação.
 */
#ifndef HASH_H
#define HASH_H

/* Bucket
 * define um espaço na tabela hashtable */
typedef struct {
  unsigned hop; /* informação de salto, ver referências */
  void *data;   /* objecto */
} Bucket;

/* HT
 * define o tipo de dados HT (ou HashTable) */
typedef struct {
  Bucket *data;               /* array de espaços na tabela */
  /* prehash
   * ponteiro para função que mapeia uma chave a um numero o mais
   * bem distribuido possivel dentro do universo de combinações
   *
   * primeiro argumento - chave
   * retorna o número */
  unsigned (*prehash)(void*);
  /* getKey
   * ponteiro para função que obtém uma chave a partir de um objecto
   *
   * primeiro argumento - objecto
   * retorna a chave */
  void *(*getKey)(void*);
  /* compare
   * ponteiro para função que compara um par de chaves,
   * e devolve zero se o valor das chaves for igual
   *
   * primeiro argumento - primeira chave
   * segundo argumento - segunda chave
   * retorna 0 se o valor das chaves for igual */
  int (*compare)(void*,void*);
  unsigned m, /* número de espaços na tabela */
           n, /* número de objectos na tabela */
           a, b; /* números aleatórios que representam
                    uma função aleatória que são gerados para
                    assegurar uma distribuição aceitável dos
                    objectos pelos espaços da tabela */
} HT;

/* freeHT
 * liberta o espaço ocupado por uma HT, mas não
 * os objectos (elementos) que a compõem
 *
 * ht - ponteiro para a HT
 *
 * nota: esta função não foi necessária nesta fase,
 * porque as tabelas utilizadas são usadas no programa todo */
void freeHT(HT *ht);

/* newHT
 * cria uma nova tabela hash
 *
 * getKey, compare, prehash, m -
 *  ver documentação da estrutura HT
 *
 * devolve uma nova tabela hash */
HT *newHT(
    void *getKey(void*),
    int compare(void*,void*),
    unsigned prehash(void*), unsigned m);

/* newStrHT
 * cria uma nova tabela hash, atribuindo automáticamente
 * o compare e o prehash relativos a chaves do tipo
 * array de caracteres (char*)
 *
 * nota: esta função não foi necessária nesta fase,
 * pois não existe necessidade de declarar uma HT de strings
 *
 * devolve uma nova tabela hash */
HT *newStrHT(void *getKey(void*));

/* newNumHT
 * cria uma nova tabela hash, atribuindo automáticamente * o compare e o prehash relativos a chaves do tipo número
 * (no máximo inteiro) sem sinal (utilizada para os ids
 * dos estudantes e dos funcionários)
 * 
 * devolve uma nova tabela hash */
HT *newNumHT(void *getKey(void*));

/* insert
 * insere um novo objecto na tabela
 *
 * ht - tabela (por referência)
 * value - objecto a inserir */
void insert(HT **ht, void *value);

/* get
 * obtém um objecto da tabela a partir de uma chave
 *
 * ht - tabela
 * key - chave
 * retorna o objecto, ou NULL se este não existir */
void *get(HT *ht, void *key);

/* del
 * remove o objecto com a chave indicada da tabela
 *
 * ht - tabela
 * key - chave
 * retorna o objecto removido ou NULL se este não existir */
void *del(HT *ht, void *key);

#include <stdio.h>
/* loadHT
 * carrega de um ficheiro elementos de uma tabela
 * e coloca-os na tabela indicada.
 *
 * ht - endereço da tabela (tabela por referência)
 * loadElement
 *  ponteiro para função que carrega apenas um
 *  objecto a partir do ficheiro apontado
 *  (primeiro argumento) e o devolve
 * fp - ponteiro para um ficheiro aberto para leitura */
void loadHT(HT **ht, void *loadElement(FILE*), FILE *fp);

/* saveHT
 * grava num ficheiro os elementos da tabela indicada
 *
 * ht - tabela
 * saveElement
 *  ponteiro para uma função que grava um objecto
 *  (primeiro argumento) no ficheiro apontado (segundo argumento)
 * fp - ponteiro para um ficheiro
 *
 * nota: gravar tabelas hash é um processo que requer
 * uma aprendizagem mais aprofundada do que tive tempo
 * de adquirir para que seja feito eficientemente, esta função
 * é lenta. */
void saveHT(HT *ht, void saveElement(void*,FILE*), FILE *fp);

#include "SortedArray.h"
/* link
 * insere (de certa forma, associa) elementos de um
 * SortedArray (SA), a uma HashTable. pareceu-me prático nesta
 * primeira fase implementar esta função.
 *
 * sa - ponteiro para um SortedArray (SA)
 * ht - tabela por referência */
void link(SA *sa, HT **ht);

#endif
