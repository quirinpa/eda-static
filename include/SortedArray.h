/* SortedArray.h
 *
 * Possui a declaração do tipo de dados SA
 * (ou SortedArray), e de todas as funções
 * relacionadas com o seu uso, de forma generalista.
 *
 * Serve ara armazenar ponteiros para qualquer
 * tipo de dados, para permitir que estes sejam listados
 * ordenadamente, e também que as pesquisas sejam o
 * mais rápidas possível (pesquisa binária).
 */
#ifndef SortedArray_H
#define SortedArray_H

/* SA
 * define o tipo de dados SA (ou SortedArray) */
typedef struct {
  void **elements;  /* array de elementos do SortedArray */
  unsigned n,       /* número de elementos actual */
           m,       /* número de espaços total */
           minSize; /* tamanho mínimo que o SA
                       pode ter (tamanho inicial) */
  /* access (semelhante ao getKey das HTs
   * ponteiro para função que obtem uma "chave"
   * a partir de um "objecto", cuja definição é
   * semelhante à que está explicada no cabeçalho
   * das HashTables
   *
   * primeiro argumento - Objecto */
  void *(*access)(void *);

  /* compare
   * semelhante ao compare das HTs, mas devolve
   * um inteiro que pode ser:
   * 0 se as chaves forem iguais
   * negativo se a primeira for menor que a segunda
   * positivo caso contrário. */
  int (*compare)(void*, void*);
} SA;

/* newSA
 * cria um novo SA (ver definição do tipo de dados) */
SA *newSA(
    void *access(void*),
    int compare(void*, void*),
    unsigned minSize);

/* search
 * utiliza a função compare para pesquisar uma
 * query no SA que é passado como primeiro argumento,
 * e devolve o ìndice do elemento encontrado, ou o
 * índice da posição onde deveria ser inserido para o
 * SA continuar ordenado. Estabelece a variàvel global
 * wasFound (definida no SortedArray.c) como true ou false (Bool) */
unsigned search( SA *sa, void *query, int compare(void*, void*));

/* find
 * utiliza a função compare do SA indicado para fazer
 * "search" da chave indicada, e devolve o seu resultado. */
unsigned find(SA *sa, void *key);

/* sortedInsert
 * insere element (Objecto), em sa e devolve
 * o índice de inserção. */
unsigned sortedInsert(SA *sa, void *element);

/* findAndRemove
 * faz "find" com a query (Chave) indicada,
 * se encontrar um elemento, remove-o do SA
 * e devolve-o, caso contrário, retorna NULL */
void *findAndRemove(SA *sa, void *query);

/* freeSA
 * liberta o espaço ocupado por sa e os seus
 * elementos, com a ajuda da função freeElement,
 * que liberta o espaço de um elemento só. */
void freeSA(SA *sa, void freeElement(void*));

#include <stdio.h>
/* saveSA
 * guarda um SA no ficheiro apontado por fp,
 * com a ajuda da função apontada por saveData,
 * que guarda apenas um elemento. (estou a ficar sem tempo) */
void saveSA(
    SA *sa,
    void saveData(void *, FILE*),
    FILE *fp);

/* loadSA
 * carrega um SA do ficheiro indicado por fp,
 * inicializando-o com os ponteiros para as funções
 * indicadas, e com o tamanho inicial igual ao numero
 * de elementos indicado no ficheiro, mais uma margem
 * (ver definição de SA). */
SA *loadSA(
    void *loadData(FILE*),
    void *access(void*),
    int compare(void*, void*),
    FILE *fp);

#endif
