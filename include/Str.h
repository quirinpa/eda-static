#ifndef STRING_H
#define STRING_H

#include <stdio.h>

typedef size_t StrLen;
typedef struct sStr {
  StrLen len;
  char *chars;
} Str;

void saveStr(Str *s, FILE *fp);
Str *loadStr(FILE *fp);
void freeStr(Str *s);

Str *StrFromCharPtr(const char *s) __attribute__ ((deprecated));
#define cs(name, s) static const Str name = { sizeof(s), (char*)s }

int cmpStr(Str *a, Str *b);

#endif
