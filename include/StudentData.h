/* StudentData.h
 *
 * Declara o tipo de dados StudentData
 * e funções associadas */
#ifndef STUDENTDATA_H
#define STUDENTDATA_H

#include "SortedArray.h"
typedef struct {
  unsigned char course;
  SA *meals; /* SortedArray das refeições */
} StudentData;

StudentData *loadStudentData(FILE *fp);
void saveStudentData(StudentData *sd, FILE *fp);
void freeStudentData(StudentData *sd);

StudentData *StudentDataInterface(void);

#endif
