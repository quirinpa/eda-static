/* User.h
 *
 * Declara o tipo de dados do utilixador, juntamente
 * com as funções associadas a esse tipo de dados. */
#ifndef USER_H
#define USER_H

#include "address.h"
#include "Bool.h"
#include "Str.h"

typedef struct {
  unsigned long id;
  Str *password;
  Str *name;
  void *utData; /* aponta para um unsigned char
                   representativo das permissões se
                   o utilizador for funcionário(a),
                   ou para StudentData no caso de ser
                   estudante. */
  unsigned short birthday;
  Address *address;
  unsigned long tel; /* número de telefone */
  unsigned short plafond;
} User;

#include <stdio.h>
void *loadStudent(FILE *fp);
void *loadEmployee(FILE *fp);
void saveStudent(void *v, FILE *fp);
void saveEmployee(void *v, FILE *fp);
void freeStudent(void *v);
void freeEmployee(User *u);

#endif
