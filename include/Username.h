#ifndef USERNAME_H
#define USERNAME_H

#include "Bool.h"
typedef struct sUsername {
  Bool isStudent;
  unsigned long id;
} Username;

Username *usernameInterface(const char *prompt);

#endif
