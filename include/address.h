/* Address.h
 *
 * Aqui está declarado o tipo de dados referente às moradas,
 * e funções associadas. Para criar uma morada através de uma
 * interface, guardá-las e carregá-las de ficheiros,
 * e libertálas da memória.
 *
 */

#ifndef ADDRESS_H
#define ADDRESS_H

#include "Str.h"
/* Address
 * representa uma morada */
typedef struct {
  Str *street;        /* rua da morada */
  unsigned char door; /* numero da porta */
  short cp;           /* codigo postal */
} Address;

/* addressInterface
 * pede uma morada ao utilizador por meio de uma interface,
 *
 * retorna um ponteiro para a nova morada. */
Address *addressInterface(void);

#include <stdio.h>
/* saveAddress
 * guarda uma morada num ficheiro
 *
 * Address *a - endereço da morada
 * FILE *fp - endereço do ficheiro */
void saveAddress(Address *a, FILE *fp);

/* loadAddress
 * carrega uma morada de um ficheiro
 *
 * FILE *fp - endereço do ficheiro
 * retorna um ponteiro para a morada carregada. */
Address *loadAddress(FILE *fp);

/* freeAddress
 * liberta o espaço em memória relativo a uma morada
 *
 * Address *a - endereço da morada */
void freeAddress(Address *a);

#endif
