/* cp.h
 *
 * aqui poderá encontrar as declarações associadas
 * aos códigos postais, que são guardados na estrutura User
 * na forma de um short int
 */

#ifndef CP_H
#define CP_H

#include <limits.h>
#define ERR_CP SHRT_MAX
/* Cp
 * cria um novo codigo postal a partir
 * dos seus dois conjuntos de digitos
 *
 * a - primeiros quatro dígitos
 * b - ùltimos três dígitos
 *
 * retorna um short representativo do código postal */
short Cp(unsigned short a, unsigned short b);

/* cpA
 * retorna os primeiros quatro dígitos a partir
 * de um short representativo de um código postal */
unsigned short cpA(short cp);

/* cpB
 * retorna os últimos três dígitos a partir
 * de um short representativo de um código postal */
unsigned short cpB(short cp);

/* cpInterface
 * Apresenta uma interface para que o utilizador gere
 * um código postal, e retorna um short int representativo
 * do mesmo */
short cpInterface(void);

#endif
