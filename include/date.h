#ifndef DATE_H
#define DATE_H

void initToday(void);

struct sDate {
  short y;
  char m, d;
};

struct sDate getSDate(unsigned short d);

#include "Bool.h"
unsigned short dateInterface(const char *prompt, Bool fromToday);
void getDateStr(unsigned short d, char *saveStr);
#endif
