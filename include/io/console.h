/* console.h
 *
 * Em resumo, as funções e procedimentos aqui definidos
 * lidam com o input e output do programa.
 *
 * Inicialmente, este ficheiro foi criado para ultrapassar as
 * inconsistencias dos metodos de leitura do stdin para o efeito
 * de interagir com o utilizador, e as respectivas
 * incompatibilidades de plataforma para plataforma.
 *
 * Inspiracao:
 * gnuwin32.sourceforge.net/packages/readline.htm
 *
 * Quem inventou IOStream? a opinião da comunidade:
 * stackoverflow.com/questions/2753060/who-architected-designed-cs-iostreams-and-would-it-still-be-considered-wel
 */
#ifndef CONSOLE_H
#define CONSOLE_H

#include "Str.h"
void printTitle(const char *t);
void printError(const char *msg);
void printStatus(const char *msg);
void printSysError(const char *msg);
void phaseOne(void);

Str *lread(const char *prompt);

unsigned long long StrToULL(Str *s,
    unsigned long long minv,
    unsigned long long maxv,
    StrLen start, StrLen *saveEnd);

unsigned long long lrULL(const char *prompt,
    unsigned long long minv,
    unsigned long long maxv);

void menuopt(unsigned long long n, const char *s);
/* #include <stdarg.h> */
unsigned char menu(const char *prompt, unsigned char nOptions, ...);

#include "Bool.h"
Bool ynmenu(const char *prompt);

#endif
