#ifndef EMPLOYEE_MENU_H
#define EMPLOYEE_MENU_H

#include "User.h"
#include "HashTable.h"
#include "SortedArray.h"
void employeeMenu(User *me,
    HT *studentIdsHT,
    HT *employeeIdsHT,
    SA *studentNamesSA);

#endif
