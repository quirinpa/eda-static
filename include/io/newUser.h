#ifndef IO_NEWUSER
#define IO_NEWUSER

#include "User.h"
#include "HashTable.h"
#include "SortedArray.h"
User *newUserInterface(
    User *employee,
    HT *studentIdsHT,
    HT *employeeIdsHT,
    SA *studentNamesSA);

#endif
