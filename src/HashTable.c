#include "HashTable.h"
/* FONTE PRINCIPAL:
 * youtube.com/watch?v=0M_klqhwbFo
 * 
 * mas nao posso usar listas. tudo bem.
 * en.wikipedia.org/wiki/Open_addressing
 */

/* UNIVERSAL HASHING
 * probability of two different keys having
 * the same hash is 1/m for worst case random h->a & h->b */
static const unsigned biggestPrime = 4294967291;
/* www.bigprimes.net/archive/prime/2032803
 * www.tutorialspoint.com/cprogramming/c_data_types.htm */

static unsigned hash(HT *ht, void *key) {
  unsigned k = ht->prehash(key);
  /* MAD - Multiply Add Divide AKA Universal Hashing
   * youtube.com/watch?v=0M_klqhwbFo#t=48m03s
   * youtube.com/watch?v=KW0UvOW0XIo#t=19m22s
   * youtube.com/watch?v=z0lJ2k0sl1g#t=9m30s
   * Pr(k1!=k2) {h(k1)=h(k2)} = 1/m
   */
  return ((ht->a*k + ht->b) % biggestPrime) % ht->m;
}

/* en.wikipedia.org/wiki/Hopscotch_hashing */
/* cs.nyu.edu/~lerner/spring11/proj_hopscotch.pdf */
/* sites.google.com/site/cconcurrencypackage/hopscotch-hashing */
/* #define H sizeof(unsigned)*8 */
#define H 32
/* #define H 4 */
static const unsigned Hm1 = H-1;
static unsigned shrinkLimit = H*4;
/* static const unsigned maxHop = 15; */
static const unsigned maxHop = 0U - 1U;

#include <time.h>
#include <stdlib.h>
/* #include <stdio.h> */
HT *newHT(
    void *getKey(void*),
    int compare(void*,void*),
    unsigned prehash(void*), unsigned m) {
  unsigned i = 0;
  HT *ht = (HT*) malloc(sizeof(HT));
  ht->n = 0;
  ht->m = m; /* https://en.wikipedia.org/wiki/Hash_table#Hashing masking */
  srand((unsigned)time(NULL));
  ht->a = 1U+(unsigned)rand();
  ht->b = 1U+(unsigned)rand();
  /* printf("NEWHT A %u B %u\n", ht->a, ht->b); */
  ht->getKey = getKey;
  ht->compare = compare;
  ht->prehash = prehash;

  ht->data = (Bucket*) malloc(ht->m * sizeof(Bucket));
  do {
    Bucket *b = &ht->data[i++];
    b->hop = 0;
    b->data = NULL;
  } while (i<ht->m);

  return ht;
}

/* static void dprint(HT *ht) { */
/*  unsigned i = 0, j; */
/*  printf("\n"); */
/*  while (i<ht->m) { */
/*    putchar('|'); */
/*    if (ht->data[i].data) */
/*      for (j=0; j<4; j++) */
/*        putchar(((char*)ht->data[i].data)[j]); */
/*    else */
/*      for (j=0; j<4; j++) */
/*        putchar(' '); */
/*    i++; */
/*  } */
/*  printf("|\n"); */
/*  i = 0; */
/*  while (i<ht->m) { */
/*    putchar('|'); */
/*    for (j=0; j<4; j++) */
/*      putchar('0' + ((ht->data[i].hop>>j)&1U)); */
/*    i++; */
/*  } */

/*  printf("|\n"); */
/* } */

/* static void dprint(HT *ht) { */
/*  unsigned i = 0; */
/*  while (i<ht->m) */
/*    if (ht->data[i++].data) printf("▓"); */

/*    else printf("░"); */

/*  putchar('\n'); */
/* } */

void freeHT(HT *ht) {
  free(ht->data);
  free(ht);
}

#include "io/console.h"
static HT *rehash(HT *ht, unsigned m) {
  phaseOne(); 
  return NULL;
  /* /1* initially, i was rehashing inside the same table, but */
  /*  * that doesn't work because there may be rehashes inside rehashes. *1/ */
  /* HT *nht = newHT(ht->getKey, ht->compare, ht->prehash, m); */
  /* unsigned i, j; */

  /* /1* printf("START REHASH load %f. ", (double)ht->n/(double)ht->m); *1/ */
  /* /1* dprint(ht); *1/ */
  /* /1* initialize new buckets *1/ */

  /* /1* save old buckets *1/ */
  /* for (i=0, j=0; i<ht->m; i++) { */
  /*  void *cdata = ht->data[i].data; */
  /*  if (cdata) { */
  /*    insert(&nht, cdata); */
  /*    j++; */
  /*  } */
  /* } */

  /* freeHT(ht); */

  /* /1* printf("END REHASH load %f ", (double)nht->n/(double)m); *1/ */
  /* /1* dprint(nht); *1/ */
  /* return nht; */
}

static HT *grow(HT *ht) {
  return rehash(ht, 2*ht->m);
}

static HT *shrink(HT *ht) {
  return rehash(ht, ht->m/2);
}

/* inicialmente, a funcao seguinte foi copiada do link
 * www.cse.yorku.ca/~oz/hash.html
 * sem que eu percebesse o seu comportamento em detalhe.
 * No entanto, tive bases para aprofundar esse conhecimento,
 * com a ajuda das aulas de MNIO (metodo de horner), e com a ajuda de:
 * youtube.com/watch?v=0M_klqhwbFo#t=45m23s
 *
 * apercebi-me (finalmente) como e que as
 * operacoes aritmeticas sao implementadas internamente.
 *
 * inicialmente, re-implementei com o metodo de horner
 * simples, multiplicando o hash anterior por 33 que e
 * um "numero magico" obtido experimentalmente que evita
 * demasiadas colisoes, conforme:
 * www.youtube.com/watch?v=KW0UvOW0XIo#t=11m10s
 *
 * depois, apercebi-me que:
 * (na expressao seguinte, as incognitas de 'a' a 'h' sao bits)
 *
 *            abcdefgh
 *             x100001(33 em binario)
 *           ----------
 *            abcdefgh
 *     + abcdefgh
 *     ----------------
 *
 * e' a mesma coisa que (abcdefgh << 5) + abcdefgh
 * e tem a vantagem de evitar overflows em relacao
 * a' multiplicacao aritmetica. Portanto:
 */
static unsigned prehashStr(void *key) {
  unsigned char *str = (unsigned char*) key;
  unsigned hash = (unsigned)*str, c;
  while ((c = (unsigned)*str++))
    hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
  return hash;
}

#include <string.h>
static int compareStr(void *a, void *b) {
  return strcmp((char*)a, (char*)b);
}

HT *newStrHT(void *getKey(void*)) {
  return newHT(getKey, compareStr, prehashStr, shrinkLimit);
}

/* outros recursos:
 * citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.85.6106&rep=rep1&type=pdf 
 * en.wikipedia.org/wiki/Horner%27s_method 
 */

static unsigned prehashNum(void *key) {
  return *(unsigned*)key;
}

static int compareNum(void *a, void *b) {
  return *(int*)b-*(int*)a;
}

HT *newNumHT(void *getKey(void*)) {
  return newHT(getKey, compareNum, prehashNum, shrinkLimit);
}

/* extern void grow(HT *ht); */
static unsigned myNextSlot(unsigned hop, unsigned startbit) {
  unsigned i = startbit;
  /* printf("my little spot\n"); */
  if (!hop) return i;
  else {
    unsigned mask = 1;

    while (!(hop & mask)) {
      mask<<=1;
      /* printf("hop%u mask%u\n", hop, mask); */
      i++;
    }

    return i;
  }
}

static unsigned nextFree(HT *ht, unsigned start) {
  /* the result of this may not fit the hash table,
   * and if it is equal to start, there is no other
   * bucket available (should grow the table) */
  unsigned j = 1;
  unsigned ji = j + start;
  while (j<ht->m) { /* can make max distance lower */
    if (!ht->data[ji % ht->m].data) return ji;
    ji++;
    j++;
  }

  return start;
}

/* be CAREFUL! this assumes that the key is not
 * already in the table! because if not, we would
 * have a performance hit! i think.. */
/* FIXME rehashes inside rehashes */
static const double tooSmall = .75;
void insert(HT **htptr, void *value) {
  HT *ht = *htptr;
  if ((double)ht->n/(double)ht->m > tooSmall) {
    /* printf("load is high. rehashing\n"); */
    *htptr=grow(ht);
    insert(htptr, value);
  } else {
    unsigned i = hash(ht, ht->getKey(value));
    Bucket *b = &ht->data[i]; 
    /* printf("%u ", i); */
    /* printf("i: %u\n", i); */
    if (b->data) {
      if (b->hop == maxHop) {
        /* printf("maxhop is reached. rehashing\n"); */
        /* *htptr = rehash(ht, ht->m); */
        *htptr=grow(ht);
        insert(htptr, value);
        return;
      } else {
        /*  printf("fullhop!! is input random?\n"); */
        unsigned fID = nextFree(ht, i); /* next free ID (may not fit) */
        if (fID == i) { /* did not fit anywhere */
          /* printf("Well.. this shouldn't happen. check the value of 'tooSmall'.\n"); */
          *htptr = grow(ht); /* might also grow on high loads to avoid O(n) time,
                                for example m/2 or 2m/3 and shrink when loads
                                get lower ex m/3 (keep in mind i am using an m
                                that is a power of two. and we dont want to be
                                constantly resizing the table */
          insert(htptr, value);
          return;
        } else {
          unsigned d = fID - i;
          if (d < H) {
            ht->data[fID % ht->m].data = value;
            b->hop |= 1U<<d;
            ht->n++;
            /* printf("semi-cool '%s' at %d hop %d load %f ", (char*)value, fID % ht->m, b->hop, (double)ht->n/(double)ht->m); */
            /* dprint(ht); */
          } else {
            Bucket *fb = &ht->data[fID % ht->m], *nfb, *auxb;
            unsigned auxID, nfID, nextSlot, k=0;

            while ((d = fID - i) > Hm1) {
              /* unsigned mask = 1U; */

              /* get bucket H-1 places before the free bucket */
              if (fID<Hm1) fID = ht->m + fID;
              auxID = fID - Hm1;
              auxb = &ht->data[auxID % ht->m]; 
              /* if it does not have hop info, get one that does */
              while (!auxb->hop && auxID<fID)
                auxb = &ht->data[(++auxID) % ht->m]; 

              if (auxID == fID) {
                /* printf("no hoppable. rehashing\n"); */
                /* *htptr = rehash(ht, ht->m); */
                *htptr=grow(ht);
                insert(htptr, value);
                return;
              }
              /* switch next occupied slot of that bucket with the free space */
              /* while can switch those bucket's slots */

              nextSlot = myNextSlot(auxb->hop, 0);
              nfID = auxID + nextSlot;
              if (nfID>fID) {
                /* printf("no hoppable (2). rehashing\n"); */
                /* *htptr = rehash(ht, ht->m); */
                *htptr=grow(ht);
                insert(htptr, value);
                return;
              }
              /* printf("fID: %u, auxID: %u, nfID: %u, nextSlot: %u, d: %u i:%u m:%u\n", fID, auxID, nfID, nextSlot, d, i, ht->m); */
              nfb = &ht->data[nfID % ht->m];
              fb->data = nfb->data;
              /* updata bucket hop information */
              auxb->hop = (auxb->hop^(1U<<nextSlot)) | (1U<<(fID-auxID));

              /* iterate */
              fID = nfID;
              fb = nfb;
              k++;
            }

            fb->data = b->data;
            b->data = value;
            b->hop |= 1U<<d;
            ht->n++;
            /* printf("uncool '%s' at %d hop %d load %f %d jumps ", (char*)value, i+d, b->hop, (double)ht->n/(double)ht->m, k); */
            /* dprint(ht); */
          }
        }
      }
    } else {
      b->data = value;
      b->hop = 1;
      ht->n++;
      /* printf("cool '%s' at %d load %f ", (char*)value, i, (double)ht->n/(double)ht->m); */
      /* dprint(ht); */
    }
  }
}

/* easy there cowbombocas */
void *get(HT *ht, void *key) {
  unsigned i = hash(ht, key),
           j = 0, mask = 1;

  Bucket *b = &ht->data[i];

  /* printf("%u %s ", i, (char*)key); */
  while (j<H) {
    if ((b->hop & mask)) {
      Bucket *fb = &ht->data[i%ht->m];
      /* printf("GET: try slot %u %s\n", j, (char*)ht->getKey(fb->data)); */
      if (!ht->compare(key, ht->getKey(fb->data))) return fb->data;
    }
    mask <<= 1U;
    j++;
    i++;
  }
  return NULL;
}


static const double tooBig = .33;
void *del(HT *ht, void *key) {
  unsigned i = hash(ht, key),
           j = 0, mask = 1;

  Bucket *b = &ht->data[i];

  /* printf("%u %s ", i, (char*)key); */
  while (j<H) {
    if ((b->hop & mask)) {
      Bucket *fb = &ht->data[i%ht->m];
      /* printf("GET: try slot %u %s\n", j, (char*)ht->getKey(fb->data)); */
      if (!ht->compare(key, ht->getKey(fb->data))) {
        void *data = fb->data;
        fb->data = NULL;
        b->hop ^= mask;
        ht->n--;

        if (ht->m>shrinkLimit && (double)ht->n/(double)ht->m > tooBig)
          shrink(ht);

        return data;
      }
    }
    mask <<= 1U;
    j++;
    i++;
  }
  return NULL;
}

#include <stdio.h>
void loadHT(HT **ht, void *loadElement(FILE*), FILE *fp) {
  unsigned i, c;

  fread(&c, sizeof(unsigned), 1, fp);
  for (i=0; i<c; i++)
    insert(ht, loadElement(fp));
}

void saveHT(HT *ht, void saveElement(void*,FILE*), FILE *fp) {
  unsigned i;

  fwrite(&ht->n, sizeof(unsigned), 1, fp);
  for (i=0; i<ht->m; i++) {
    void *el = ht->data[i].data;
    if (el) saveElement(el, fp);
  }
}

#include "SortedArray.h"
void link(SA *sa, HT **ht) {
  unsigned i;
  for (i=0; i<sa->n; i++)
    insert(ht, sa->elements[i]);
}
/* also researched: 
 * Cuckoo Hashing: https://www.youtube.com/watch?v=HRzg0SzFLQQ
 * http://www.mit.edu/~victorj/hash_tables_cache_performance.pdf
 * and probably some others.. i didnt copy any code (the exception was the str prehash) */
