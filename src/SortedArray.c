#include "SortedArray.h"

#include "Bool.h"
Bool wasFound; /* use this to return find status
                  so that we can reuse the find()
                  function to insert elements */

#include <stdlib.h>
SA *newSA(void *access(void*), int compare(void*, void*), unsigned minSize) {
  SA *sa = (SA*) malloc(sizeof(SA));
  sa->minSize = minSize;
  sa->m = minSize;
  sa->elements = (void**) malloc(minSize * sizeof(void*));
  sa->n = 0;
  sa->access = access;
  sa->compare = compare;
  return sa;
}

#include "Bool.h"
unsigned int search(SA *sa, void *query, int compare(void*, void*)) {
  long l = 0;
  if (sa->n) {
    long r = (long)(sa->n - 1);

    while (r >= l) {
      long m = (l+r)/2;
      int cmpr = compare(query, sa->access(sa->elements[m]));
      /* printf("M: %ld\n", m); */

      if (!cmpr) {
        wasFound = true;
        return (unsigned int)m;
      }

      /* printf("COMPR: %d\n", cmpr); */
      if (cmpr < 0) r = m - 1;
      else l = m + 1;
      /* printf("l:%ld r:%ld\n", l, r); */
    }
    if (l<0) l = r;
  }

  wasFound = false;
  return (unsigned int)l;
}

unsigned int find(SA *sa, void *key) {
  return search(sa, key, sa->compare);
}

#define SA_APPEND
#include "io/console.h"
#include <stdio.h>
unsigned int sortedInsert(SA *sa, void *element) {
  void *key = sa->access(element);
  unsigned int ex = find(sa, key), i;
  if (wasFound)
#ifdef SA_APPEND
  {
    printSysError("SA::insert: Appending after existing elements of the same key");
    while (++ex<sa->n && !sa->compare(key, sa->access(sa->elements[ex])));
  }
#else
    printSysError("SA::insert: Prepending to existing element of the same key");
#endif
  if (sa->n >= sa->m) {
    phaseOne();
    /* void **new_elements = (void**)malloc((sa->m *= 2) * sizeof(void*)); */
    /* /1* if i used realloc here, it would cause the elements on */
    /*  * the right side of the found index (if one is found) to */
    /*  * be copied twice (internally). So i copy manually. *1/ */

    /* for (i = 0; i<ex; i++) */
    /*  new_elements[i] = sa->elements[i]; */

    /* while (i<sa->n) { */
    /*  new_elements[i+1] = sa->elements[i]; */
    /*  i++; */
    /* } */

    /* free(sa->elements); */
    /* sa->elements = new_elements; */

  } else
    for (i = ex; i<sa->n; i++)
      sa->elements[i+1] = sa->elements[i];

  sa->elements[ex] = element;
  sa->n++;
  return ex;
}

static void *sortedRemove(SA *sa, unsigned int index) {
  if (index < sa->n) {
    void *data = sa->elements[index];
    unsigned int i;
    sa->n--;
    for (i = index; i<sa->n; i++)
      sa->elements[i] = sa->elements[i+1];
    return data;
  }
  return NULL;
}

void *findAndRemove(SA *sa, void *query) {
  unsigned id = find(sa, query);

  if (wasFound)
    return sortedRemove(sa, id);

  return NULL;
}

void freeSA(SA *sa, void freeElement(void*)) {
  unsigned int i = 0;
  while (i<sa->n)
    freeElement(sa->elements[i++]);
  free(sa);
}

#include <stdio.h>
void saveSA(SA *sa, void saveData(void*, FILE*), FILE *fp) {
  unsigned int i = 0;
  fwrite(&sa->n, sizeof(unsigned int), 1, fp);

  while (i < sa->n)
    saveData(sa->elements[i++], fp);
}

SA *loadSA(void *loadData(FILE *),
  void *access(void*), int compare(void*, void*), FILE *fp) {

  SA *sa = (SA*) malloc(sizeof(SA));
  unsigned int i;

  sa->access = access;
  sa->compare = compare;

  fread(&sa->n, sizeof(unsigned int), 1, fp);

  /* sa->m = sa->n; */
  sa->m = sa->n + 10;
  sa->elements = (void **) malloc(sa->m * sizeof(void*));

  for (i = 0; i<sa->n; i++)
    sa->elements[i] = loadData(fp);

  return sa;
}
