#include "Str.h"
#include <stdio.h>

void saveStr(Str *s, FILE *fp) {
  fwrite(&s->len, sizeof(StrLen), 1, fp);
  fwrite(s->chars, sizeof(char), s->len, fp);
}

#include <stdlib.h>
Str *loadStr(FILE *fp) {
  Str *s = (Str*) malloc(sizeof(Str));

  fread(&s->len, sizeof(StrLen), 1, fp);
  s->chars = (char*) malloc((size_t)(s->len + 1) * sizeof(char));
  fread(s->chars, sizeof(char), s->len, fp);
  s->chars[s->len] = '\0';
  return s;
}

#include <string.h>
Str *StrFromCharPtr(const char *o) {
  Str *s = (Str*) malloc(sizeof(Str));

  s->len = (StrLen) strlen(o);
  s->chars = (char*) malloc((size_t)s->len * sizeof(char));
  strcpy(s->chars, o);

  return s;
}

void freeStr(Str *s) {
  free(s->chars);
  free(s);
}

int cmpStr(Str *a, Str *b) {
  StrLen minL = a->len > b->len ? a->len : b->len, i = 0;
  while (i<minL) {
    int d = a->chars[i] - b->chars[i];
    if (d) return d;
    i++;
  }
  return 0;
}
