#include "StudentData.h"

#include <stdio.h>
static void *loadMeal(FILE *fp);
static void *accessMeal(void *m);
static int compareMeal(void *a, void *b);

#include <stdlib.h>
#include "SortedArray.h"
StudentData *loadStudentData(FILE *fp) {
  StudentData *sd = (StudentData*) malloc(sizeof(StudentData));

  fread(&sd->course, sizeof(char), 1, fp); 
  sd->meals = loadSA(loadMeal, accessMeal, compareMeal, fp);

  return sd;
}

static void saveMeal(void *m, FILE *fp);

void saveStudentData(StudentData *sd, FILE *fp) {
  fwrite(&sd->course, sizeof(char), 1, fp);
  saveSA(sd->meals, saveMeal, fp);
}

#include "io/console.h"
StudentData *StudentDataInterface(void) {
  StudentData *sd = (StudentData*) malloc(sizeof(StudentData));
  sd->course = (unsigned char)lrULL("ID do curso", 0, 255);
  /* sd->meals = newSA(accessMeal, compareMeal, 1); */
  sd->meals = newSA(accessMeal, compareMeal, 10);
  return sd;
}

static void freeMeal(void *m);

void freeStudentData(StudentData *sd) {
  freeSA(sd->meals, freeMeal);
  free(sd);
}

/* private */

static void *loadMeal(FILE *fp) {
  void *m = malloc(sizeof(long));
  fread(m, sizeof(long), 1, fp);
  return m;
}

static void *accessMeal(void *m) { return m; }

static int compareMeal(void *a, void *b) {
  return (int)(*(long long*)a-*(long long *)b);
}

static void saveMeal(void *m, FILE *fp) {
  fwrite(m, sizeof(long), 1, fp);
}

static void freeMeal(void *m) { free(m); }
