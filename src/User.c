#include "User.h"

#include <stdio.h>
static User *loadUser(FILE *fp);

#include "StudentData.h"
void *loadStudent(FILE *fp) {
  User *u = loadUser(fp);
  u->utData = loadStudentData(fp);
  return (void*)u;
}

#include <stdlib.h>
void *loadEmployee(FILE *fp) {
  User *u = loadUser(fp);
  u->utData = malloc(sizeof(char));
  fread(u->utData, sizeof(char), 1, fp);
  return (void*)u;
}

static void saveUser(User *u, FILE *fp);

void saveStudent(void *v, FILE *fp) {
  User *u = (User*)v;
  saveUser(u, fp);
  saveStudentData((StudentData*)u->utData, fp);
}

void saveEmployee(void *v, FILE *fp) {
  User *u = (User*)v;
  saveUser(u, fp);
  fwrite(u->utData, sizeof(char), 1, fp);
}

#include "Str.h"
void freeStudent(void *v) {
  User *u = (User*)v;
  freeStr(u->name);
  freeStr(u->password);
  freeStudentData((StudentData*)u->utData);
  free(u);
}

void freeEmployee(User *u) {
  freeStr(u->name);
  freeStr(u->password);
  free(u->utData);
  free(u);
}

static User *loadUser(FILE *fp) {
  User *u = (User*) malloc(sizeof(User));
  fread(&u->id, sizeof(long), 1, fp);
  fread(&u->birthday, sizeof(short), 1, fp);
  fread(&u->tel, sizeof(long), 1, fp);
  fread(&u->plafond, sizeof(short), 1, fp);
  u->password = loadStr(fp);
  u->name = loadStr(fp);
  return u;
}

static void saveUser(User *u, FILE *fp) {
  fwrite(&u->id, sizeof(long), 1, fp);
  fwrite(&u->birthday, sizeof(short), 1, fp);
  fwrite(&u->tel, sizeof(long), 1, fp);
  fwrite(&u->plafond, sizeof(short), 1, fp);
  saveStr(u->password, fp);
  saveStr(u->name, fp);
}
