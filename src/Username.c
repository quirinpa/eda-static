#include "Username.h"

#include "Bool.h"
#include "Str.h"
#include <errno.h>
#include <stdlib.h>
#include "io/console.h"
Username *usernameInterface(const char *prompt) {
  Username *username = (Username*) malloc(sizeof(Username));
  Str *usernameStr;

repeatLogin:
  usernameStr = lread(prompt);

  if (usernameStr->len<8)
    goto invalidLogin;

  if (usernameStr->chars[0] == 'a')
    username->isStudent = true;
  else if (usernameStr->chars[0] == 'f')
    username->isStudent = false;
  else goto invalidLogin; 

  username->id = (unsigned long)StrToULL(usernameStr, 1000000, 9999999, 1, NULL);
  /* printf("got id %u\n", username->id); */

  if (errno)
    goto invalidLogin;

  freeStr(usernameStr);
  return username;

invalidLogin:
  printError("O login que introduziu e invalido.\nDevera introduzir o carater 'a' ou 'f'\nseguido de 7 digitos numericos.");
  freeStr(usernameStr);
  goto repeatLogin;
}

