#include "address.h"

#include <stdlib.h>
#include <stdio.h>

#include "cp.h"
#include "Str.h"
#include "io/console.h"

Address *addressInterface() {
  Address *r = (Address*) malloc(sizeof(Address));

  printf("Morada:\n");
  r->street = lread(" Rua");
  r->door = (unsigned char)lrULL(" Numero de Porta", 0, 255);
  r->cp = cpInterface();

  return r;
}

void saveAddress(Address *a, FILE *fp) {
  saveStr(a->street, fp);
  fwrite(&a->door, sizeof(char), 1, fp);
  fwrite(&a->cp, sizeof(short), 1, fp);
}

Address *loadAddress(FILE *fp) {
  Address *a = (Address*) malloc(sizeof(Address));
  a->street = loadStr(fp);
  fread(&a->door, sizeof(char), 1, fp);
  fread(&a->cp, sizeof(short), 1, fp);
  return a;
}

void freeAddress(Address *a) {
  free(a->street);
  free(a);
}
