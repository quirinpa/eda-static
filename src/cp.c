#include "cp.h"

#define nCp4dig 41
/* o programa só permite códigos postais madeirenses,
 * e estes são os ùnicos possíveis. Este é um efeito
 * segundário da minha tentativa de escrever ficheiros
 * bit a bit, da qual desisti por me ter de focar noutras
 * facetas do projeto, quando me deparei com a dificuldade
 * de que dependendo da arquitectura e do compilador,
 * a Endianness poderia diferir */
static const unsigned short cp4dig[nCp4dig] = {
  9000, 9001, 9004, 9020, 9021, 9024, 9030, 9050,
  9051, 9054, 9060, 9061, 9064, 9100, 9101, 9125,
  9126, 9135, 9136, 9200, 9201, 9225, 9226, 9230,
  9231, 9240, 9241, 9270, 9271, 9300, 9301, 9304,
  9325, 9326, 9350, 9351, 9360, 9361, 9370, 9371, 9385
};

/* dados os primeiros quatro dígitos do código postal,
 * devolve a posicão dos mesmos no array acima declarado. */
static unsigned char getCp4digId(unsigned short a) {
  /* pesquisa binaria */
  unsigned char l = 0, r = nCp4dig - 1;
  while (r >= l) {
    unsigned char m = (unsigned char)((l+r)/2);
    if (a == cp4dig[m]) return m;
    if (a < cp4dig[m]) r = (unsigned char)(m-1);
    else l = (unsigned char)(m + 1);
  }
  return nCp4dig;
}

/* cria uma short que contém tudo o que é preciso para
 * representar um código postal madeirense, dados os
 * primeiros quatro e os últimos três dígitos do código postal */
short Cp(unsigned short a, unsigned short b) {
  if (b>999 || a<9000 || a>9999) return ERR_CP;
  else {
    unsigned char rA = getCp4digId(a);
    if (rA==nCp4dig) return ERR_CP;
    return (short)((b<<6)|rA);
  }
}

/* segundo uma short representativa de um CP, quais os
 * seus primeiros quatro dígitos? */
unsigned short cpA(short cp) { return cp4dig[cp&63]; }

/* segundo uma short representativa de um CP, quais os
 * seus últimos três dígitos? */
unsigned short cpB(short cp) { return (unsigned short)(cp>>6); }

#include "io/console.h"
#include <errno.h>
/* pede um código postal ao utilizador e devolve-o */
short cpInterface() {
  Str *inp;
  short cp;
  unsigned short a, b;
  StrLen end;

invalidCp:
  inp = lread(" Codigo postal [XXXX-XXX]");

  a = (unsigned short) StrToULL(inp, 1000, 9999, 0, &end);
  if (errno != EINVAL) {
    freeStr(inp);
    printError("Os primeiros 4 dígitos do código\n"
        "postal devem ser expressos como\n"
        "um número entre 1000 e 9999");

    goto invalidCp;
  }

  b = (unsigned short) StrToULL(inp, 0, 999, end+1, NULL);

  freeStr(inp);
  if (errno) {
    printError("Os últimos 3 dígitos do código\n"
        "postal devem ser expressos como\n"
        "um número entre 0 e 999");

    goto invalidCp;
  } else if ((cp = Cp(a, b)) == ERR_CP) {
    printError("O código postal introduzido não\n"
        "existe na sua ilha actual");

    goto invalidCp;
  }

  return cp;
}
