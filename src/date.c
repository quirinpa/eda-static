#include "date.h"

#include <limits.h>
/* Este código é completamente original, e não permito a sua
 * utilização fora do ambito deste projecto sem autorização
 * prévia, assim como o restante código deste projeto.*/

/* gregorian calendar date EPOCH is 1 1 1900
 * i assume the short is limited at 65536
 * so max date is 7/6/2079 */
#define EPOCH 1900
#define EEPOCH 2078

#include <stdio.h>
/* segundo uma diferença de anos entre um valor e o
 * EPOCH, devolve o número de dias que passáram. */
static int dudy(int dy) { return dy*365 + (dy-1)/4; }

/* dado um ano, devolve o número de dias que passaram
 * desde o EPOCH */
static int duy(int y) { return dudy(y - EPOCH); } 

/* dado um mês, e um valor que é 1 ou 0, dependendo,
 * respectivamente, de se se trata de um ano bisexto ou
 * não, devolve o número de dias desde o início do ano
 * até o início desse mês. */
static int dum(int m, int l) {
  int s = 0;
  if (m>1) {
    s -= 2 - l;
    if (m>=7) {
      m -= 7;
      s += 214;
    }
  }
  return s + 31*m - m/2;
}

/* devolve o número de dias desde 1 de janeiro de 1900 (EPOCH)
 * dado um ano, mês e dia. */
static unsigned short newDate(int y, int m, int d) {
  return (unsigned short)(
      duy(y) + dum(m-1, !(y % 4)) + d-1
      );
}

/* estrutura auxiliar para quando é necessário
 * saber o ano, mês e dia de uma data frequentemente,
 * ou o dia do ano, ou se o ano é bisexto ou não,
 * respectivamente */
struct dt { int y, m, d, yd, l; };

/* devolve uma variàvel do tipo acima descrito
 * dado o número de dias desde o EPOCH */
static struct dt getDS(unsigned short dt) {
  int rd;
  struct dt r;
  r.y = 4*(dt+1)/1461;
  r.l = !(r.y % 4);
  rd = dt - dudy(r.y);
  r.d = r.yd = rd;
  r.m = 0;
  if (r.yd > 59+r.l) {
    if (r.yd >= 212+r.l) {
      r.m = 7;
      r.d -= 212 - r.l;
    } else r.d -= 2 - r.l;
  }
  r.m += 2*(r.d+1)/61;
  r.d = r.yd - dum(r.m, r.l);
  return r;
}

/* a variável que se segue (sendo declarada como extern
 * caso necessária noutro ficheiro), guarda o número de dias
 * que passaram desde a data EPOCH, e é necessário que seja
 * inicializada antes de ser usada, através da função initToday()
 * se isso não acontecer, dateInterface, getDateStr, e outras
 * funcionalidades que dependam de "today" não vão funcionar correctamente. */
unsigned short today;
#include <stdlib.h>
/* a funcáo seguinte guarda uma representação textual de um
 * número de dias desde o EPOCH no array saveStr. Assume-se
 * que tenha sido alocado espaço para pelo menos 11 caracteres. */
void getDateStr(unsigned short dt, char *saveStr) {
  if (dt == today)
    sprintf(saveStr, "hoje");
  else if (dt - 1 == today)
    sprintf(saveStr, "amanha");
  else {
    struct dt ds = getDS(dt);
    sprintf(saveStr, "%u %d/%d/%d", dt, ds.d+1, ds.m+1, ds.y + EPOCH);
  }
}

/* static void dDate(int y, int m, int d) { */
/*  char *str = (char*) malloc(11*sizeof(char)); */
/*  unsigned short dt = newDate(y, m, d); */
/*  getDateStr(dt, str); */
/*  printf("%d/%d/%d >>> %s\n", y, m, d, str); */
/* } */

#include <time.h>
/* a função seguinte inicializa a variàvel today
 * com o número de dias desde a data EPOCH */
void initToday(void) {
  time_t tToday = time(NULL);
  struct tm ltToday = *localtime(&tToday);
  today = newDate(
        (unsigned short)EPOCH + ltToday.tm_year,
        (unsigned char)ltToday.tm_mon + 1,
        (unsigned char)ltToday.tm_mday);
  /* dDate(2034,12,2); */
  /* dDate(2034,3,1); */
  /* dDate(2016,3,1); */
  /* dDate(2016,12,1); */
  /* dDate(2015,12,1); */
  /* dDate(EEPOCH,3,1); */
  /* dDate(EEPOCH,5,1); */
}

#include "io/console.h"
#include "Bool.h"
#include <errno.h>
#include <string.h>
/* pede uma data ao utilizador, e devolve o número de dias desde
 * EPOCH, prompt é o texto a aparecer antes da indicação do formato
 * da data, fromToday, se true, indica que só datas presentes ou
 * futuras são válidas. */
unsigned short dateInterface(const char *prompt, Bool fromToday) {
  Str *inp;

invalidDate:
  printf("%s ", prompt);
  inp = lread("[dd/mm/aaaa]");
  if (!strcmp("hoje", inp->chars))
    return today;
  else if (!strcmp("amanha", inp->chars))
    return (unsigned short)(today + 1);
  else {
    int d, m, y;
    unsigned short dt;
    StrLen end;

    d = (int) StrToULL(inp, 1, 31, 0, &end);

    if (d == 0 || errno != EINVAL) {
      freeStr(inp);
      printError("O dia tem que ser um numero de 1 e 31");
      goto invalidDate;
    }

    m = (int) StrToULL(inp, 1, 12, end+1, &end);

    if (m == 0 || errno != EINVAL) {
      freeStr(inp);
      printError("O mes tem que ser um numero de 1 e 12");
      goto invalidDate;
    }

    y = (int) StrToULL(inp, EPOCH, EEPOCH, end+1, NULL);

    freeStr(inp);
    if (errno) {
      printError("O ano introduzido nao e suportado pelo programa");
      goto invalidDate;
    } else {
      dt = newDate(y, m, d);
      if (fromToday && dt<today){
        printError("Datas passadas sao invalidas");
        goto invalidDate;
      }

      return dt;
    }
  }
}
