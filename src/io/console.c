#include "io/console.h"

#ifdef __linux
#include <ncurses.h>
/* #define clrscr() system("clear"); */
#define PROMPT_START "\033[94m"
#else
#ifndef __CYGWIN__
#include <conio.h>
#endif
/* #define clrscr() system("CLS"); */
#define PROMPT_START ""
#endif
#define ERROR_START "\033[91m"
#define STATUS_START "\033[92m"
#define SYSERROR_START "\033[93m"
#define TITLE_START "\033[1m"
#define RESET "\033[0m"
#include <stdio.h>
#include <stdlib.h>

#include "Str.h"
void printTitle(const char *t) { printf("\n"TITLE_START"%s"RESET"\n", t); }

void printError(const char *msg) {
  printf(ERROR_START"%s"RESET"\n\n", msg);
}

void printStatus(const char *msg) {
  printf(STATUS_START"%s"RESET"\n\n", msg);
}

void printSysError(const char *msg) {
  printf(SYSERROR_START"%s"RESET"\n\n", msg);
}

void phaseOne(void) {
  printSysError("O programa quer redimensionar um vector,\n"
      "e o codigo para isso ja esta implementado.\n"
      "no entanto, nesta primeira fase, essa funcionalidade\n"
      "nao e permitida, portanto o programa saiu.\n"
      "Ate a proxima fase!");

  exit(1);
}

static const StrLen
  LREADLINE_ISIZE = 32,
  LREADLINE_INC = 10;

Str *lread(const char *prompt) {
  Str *s = (Str*) malloc(sizeof(Str));
  StrLen realSize = LREADLINE_ISIZE;
  int c;

  s->len = 0;
  s->chars = (char*)malloc((size_t)realSize * sizeof(char));
  printf("%s: "PROMPT_START, prompt);

morechars:
  c = getchar();

  if (c == EOF) {
    freeStr(s);
#ifdef __linux
    printf(RESET);
#endif
    return NULL;
  } else if (c!='\n') {
    s->chars[s->len] = (char)c;

    if (++s->len == realSize - 1) {
      phaseOne();
      /* realSize += LREADLINE_INC; */
      /* s->chars = (char*)realloc( */
      /*    (void*)s->chars, */
      /*    (size_t)realSize * sizeof(char)); */
    }

    goto morechars;
  }

  s->chars[s->len] = '\0';
#ifdef __linux
    printf(RESET);
#endif
  return s;
}

/* Str *lreadp(char *prompt) { */
/*  /1* Str *r = lread(prompt); *1/ */
/*  /1* clrscr(); *1/ */
/*  /1* return r; *1/ */
/*  return lread(prompt); */
/* } */

#include <errno.h>
unsigned long long StrToULL(Str *s,
    unsigned long long minv,
    unsigned long long maxv, StrLen start, StrLen *saveEnd) {
  /* www.freebsd.org/cgi/man.cgi?query=strtonum */
  StrLen i = start;
  unsigned long long r = 0;

  if (!s->len) {
    errno = EINVAL;
    return r;
  }

  while (i<s->len) {
    char c = s->chars[i];
    if (c<'0' || c>'9') {
      errno = EINVAL;
      if (saveEnd) *saveEnd = i;
      return r;
    } else {
      r = r*10 + (unsigned long long)(c-'0');
      if (r>maxv) {
        errno = ERANGE;
        return r;
      }
      i++;
    }
  }

  if(r<minv) {
    errno = ERANGE;
  } else errno = 0;

  return r;
}

unsigned long long lrULL(const char *prompt,
    unsigned long long minv,
    unsigned long long maxv) {
  Str *auxStr;
  unsigned long long r;
invalidULL:
  auxStr = lread(prompt);
  r = StrToULL(auxStr, minv, maxv, 0, NULL);
  freeStr(auxStr);
  if (errno) {
    char errormsg[100];
    sprintf(errormsg, "Valor invalido. Introduza um numero de %llu a %llu", minv, maxv);
    printError(errormsg);
    goto invalidULL;
  }
  return r;
}

void menuopt(unsigned long long n, const char *s) {
  printf(" "PROMPT_START"%llu"RESET") %s\n", n, s);
}

#include <stdarg.h>
unsigned char menu(const char *prompt, const unsigned char nOptions, ...) {
  va_list options;
  unsigned char i;

  printTitle(prompt);

  va_start(options, nOptions); 

  for (i = 0; i<nOptions; i++)
    menuopt(i, va_arg(options, char*));

  va_end(options);

  return (unsigned char)lrULL("Resposta", 0, nOptions);
}

#include "Bool.h"
Bool ynmenu(const char *prompt) {
  Str *aux;
  Bool r;

  printf(prompt);

  aux = lread("? (s,n*)");
  r = aux->chars[0] == 's';
  free(aux);
  return r;
}
