#include "io/employeeMenu.h"

#include "SortedArray.h"
static void findStudentByNameInterface(SA *studentNamesSA);

static void listUsers(SA *users);

#include "HashTable.h"
static void removeStudentInterface(
    HT *studentIdsHT,
    SA *studentNamesSA);

static void consumeMealInterface(
    HT *studentIdsHT);

static void listMealsInterface(
    SA *studentNamesSA,
    HT *studentIdsHT);

#include "User.h"
#include "io/console.h"
#include "io/newUser.h"
void employeeMenu(User *me,
    HT *studentIdsHT,
    HT *employeeIdsHT,
    SA *studentNamesSA) {

  unsigned char r;

  do {
    r = menu(
        "MENU DE FUNCIONARIO", 7, "Logout",
        "Adicionar Utilizador",
        "Encontrar Aluno",
        "Listar Alunos",
        "Remover Aluno",
        "Consumir Refeicao",
        "Listar Refeicoes");

    switch (r) {
      case 1: newUserInterface(me, studentIdsHT,
                  employeeIdsHT, studentNamesSA); break;
      case 2: findStudentByNameInterface(studentNamesSA); break;
      case 3: listUsers(studentNamesSA); break;
      case 4: removeStudentInterface(studentIdsHT, studentNamesSA); break;
      case 5: consumeMealInterface(studentIdsHT); break;
      case 6: listMealsInterface(studentNamesSA, studentIdsHT);
    }
  } while (r);
}

#include <stdio.h>
static void printUser(User *u) {
  printf("a%lu - %s\n", u->id, u->name->chars);
}

#include "Str.h"
static void findStudentByNameInterface(
    SA *studentNamesSA) {

  Str *query;
  unsigned firstId, i, j;

  printTitle("ENCONTRAR ALUNO");
  query = lread("Primeiro nome do aluno");

  i = firstId = find(studentNamesSA, query);

  while (i < studentNamesSA->n) {
    User *s = (User*)studentNamesSA->elements[i];

    /* repeating compares... what a shame... */
    for (j=0; j<query->len; j++)
      if (query->chars[j] != s->name->chars[j])
        goto exitSearch;

    printUser(s);
    i++;
  }

exitSearch:
  printf("%u Resultado(s)\n", i - firstId);
}


static void listUsers(SA *users) {
  unsigned i;
  printTitle("LISTAGEM DE ALUNOS");

  for (i=0; i<users->n; i++)
    printUser((User*)users->elements[i]);

  printf("%u Registo(s)\n", i);
}

static const char *noStudentStr =
"Nao foi encontrado nenhum aluno\n"
"com esse numero mecanografico.";

static void removeStudentInterface(
    HT *studentIdsHT,
    SA *studentNamesSA) {

  User *aluno;
  unsigned long id;

  printTitle("REMOCAO DE ALUNOS");

  id = (unsigned long)lrULL("Numero do aluno", 1000000, 9999999);

  if ((aluno = (User*)del(studentIdsHT, (void*)&id))) {
    findAndRemove(studentNamesSA, aluno->name);
    freeStudent(aluno);
    printStatus("Aluno Removido\n");
  } else
    printError(noStudentStr);
}

#include <time.h>
#include "date.h"
#include <stdlib.h>
#include "StudentData.h"
extern unsigned short today;
static void consumeMealInterface(
    HT *studentIdsHT) {

  unsigned long id;
  User *student;

  printTitle("CONSUMIR REFEICAO");

  id = (unsigned long)lrULL("Numero do aluno", 1000000, 9999999);

  student = (User*) get(studentIdsHT, &id);

  if (student) {
    time_t tNow = time(NULL);
    struct tm ltNow = *localtime(&tNow);
    Bool dinner = false;

    if ((dinner = ltNow.tm_hour>19 && ltNow.tm_hour<=23) ||
        (ltNow.tm_hour>12 && ltNow.tm_hour<=15)) {

      unsigned long m = (unsigned long)(today<<1U) | dinner, *fm;
      SA *sMeals = ((StudentData*)student->utData)->meals;
      fm = (unsigned long*) findAndRemove(sMeals, &m);
      if (fm) {
        free(fm);
        printStatus("Refeicao consumida");
      } else {
        char err[100];
        sprintf(err,"O aluno nao encomendou refeicao para o %s de hoje", m & 1U ? "jantar" : "almoco");
        printError(err);
      }

      return;
    }

    printError("Nao se pode consumir refeicoes a esta hora");
  } else printError(noStudentStr);
}

#include "StudentData.h"
static unsigned int listMeals(User *student) {
  if (student) {
    SA *meals = ((StudentData*)student->utData)->meals;

    printf("%lu - %s\n", student->id, student->name->chars);
    {
      unsigned i;
      for (i=0; i<meals->n; i++) {
        unsigned long *m = (unsigned long*) meals->elements[i];
        char dateStr[11];

        getDateStr((short unsigned)(*m>>1), dateStr);
        printf(" %s - %s\n", dateStr, (*m&1) ? "jantar" : "almoco");
      }
      return i;
    }
  } else printError("O aluno nao existe");
  return 0;
}

#include <errno.h>
static void listMealsInterface(
    SA *studentNamesSA, HT *studentIdsHT) {

  unsigned long id;
  Str *inp;

listMealsAgain:
  inp = lread("Numero do aluno");

  if (inp->len == 1 && inp->chars[0] == '0') {
    unsigned int nMeals = 0;

    for (id=0; id<studentNamesSA->n; id++)
      nMeals += listMeals((User*)studentNamesSA->elements[id]);

    printf("%lu refeicoes.\n", nMeals);
    return;

  } else {

    id = (unsigned long)StrToULL(inp, 1000000, 9999999, 0, NULL);

    if (!errno) {
      printf("%lu refeicoes.\n", listMeals((User*)get(studentIdsHT, &id)));
      return;
    }

  }

  printError("Tem que introduzir um numero de aluno\n"
      "valido ou 0 para mostrar todos os alunos");
  goto listMealsAgain;
}
