#include "io/newUser.h"

#include <stdlib.h>
#include "date.h"
#include "address.h"
#include "Username.h"
#include "HashTable.h"
#include "StudentData.h"
#include "SortedArray.h"
#include "io/console.h"
User *newUserInterface(
    User *employee,
    HT *studentIdsHT,
    HT *employeeIdsHT,
    SA *studentNamesSA) {

  User *u = (User*) malloc(sizeof(User));
  Username *un;
  Str *pwdConf;

  printTitle("ADICIONAR UTILIZADOR");
  u->name = lread("Nome completo");
invalidUsername:
  un = usernameInterface("Login desejado");

  if (un->isStudent) {
    if (get(studentIdsHT, (void*)&un->id)) {
      printError("Ja existe um aluno com esse id.");
      goto invalidUsername;
    } else {
      u->utData = StudentDataInterface();
      u->id = un->id;
      insert(&studentIdsHT, u);
      sortedInsert(studentNamesSA, u);
    }
  } else {
    if (get(employeeIdsHT, (void*)&un->id)) {
      printError("Ja existe um funcionario com esse id.");
      goto invalidUsername;
    } else {
      unsigned char *perms = (unsigned char*) malloc(sizeof(char));
      u->id = un->id;
      insert(&employeeIdsHT, u);
invalidPerms:
      *perms = (unsigned char)lrULL("Permissoes", 0, 255);
      if (employee && (*perms & *(unsigned char*)employee->utData) != *perms) {
        printError("Autoridade Insuficiente");
        goto invalidPerms;
      }
      u->utData = (void*) perms;
    }
  }
  u->birthday = dateInterface("Data de nascimento", false);
  u->address = addressInterface();
  u->tel = (unsigned long) lrULL("Numero de Telefone", 100000000, 999999999);
  u->plafond = 0;

invalidpwd:
  u->password = lread("Palavra-passe");
  pwdConf = lread("Confirme a palavra-passe");
  if (cmpStr(u->password, pwdConf)) {
    freeStr(u->password);
    freeStr(pwdConf);
    goto invalidpwd;
  }

  return u;
}
