#include "io/studentMenu.h"

#include "User.h"
static void orderMealInterface(User *me);
static void depositInterface(User *me);

#include "io/console.h"
void studentMenu(User *me) {
  unsigned char r;
  do {
    r = menu(
        "MENU DE ESTUDANTE", 3,
        "Logout",
        "Encomendar Refeicao",
        "Carregar Plafond");

    switch (r) {
      case 1: orderMealInterface(me); break;
      case 2: depositInterface(me); break;
    }
  } while (r);
}

#include "date.h"
#include <time.h>
#include <stdlib.h>
#include "SortedArray.h"
#include "StudentData.h"
extern Bool wasFound;
extern unsigned short today;
static void orderMealInterface(User *me) {
  char dateStr[11];
  printTitle("ENCOMENDAR REFEICAO");

  if (me->plafond >= 3U) {
    SA *myMeals = ((StudentData*)me->utData)->meals;
    unsigned long *m = (unsigned long*)malloc(sizeof(long));
    unsigned short d;
    char status[50];
    Bool dinner;

    d = dateInterface("Para que data?", true);
    if (today == d) {
      time_t tNow = time(NULL);
      struct tm ltNow = *localtime(&tNow);

      if (ltNow.tm_hour>23) {
        printError("Passa da hora de jantar. Ja nao pode\n"
            "encomendar refeicoes para hoje.");
        return;
      }
      dinner = ltNow.tm_hour>15 || ynmenu("Jantar");
    } else dinner = ynmenu("Jantar");

    *m = (long unsigned)(d<<1U) |
      (long unsigned)dinner; 

    getDateStr(d, dateStr);
    find(myMeals, m);
    if (wasFound) {
      sprintf(status, "Ja tinha encomendado uma refeicao para o %s de %s",
          dinner ? "jantar" : "almoço", dateStr);

      printError(status);
    } else {
      me->plafond = (short unsigned)(me->plafond - 3);

      sortedInsert(myMeals, m);

      sprintf(status, "Refeicao encomendada para o %s de %s",
          dinner ? "jantar" : "almoço", dateStr);
      printStatus(status);
    }
    return;
  }

  printError("Nao tem saldo suficiente");
}

#include <limits.h>
static void depositInterface(User *me) {
  unsigned short canDeposit = (unsigned short)((unsigned)USHRT_MAX - me->plafond);

  printTitle("CARREGAR PLAFOND");

  printf("Saldo: %u creditos.\n", me->plafond);
  if (!canDeposit) {
    printError(
        "Parabens! atingiu o limite de\n"
        "saldo que o programa suporta!\n"
        "Para carregar mais saldo,\n"
        "veja se almoca primeiro, ou assim.");
    return;
  }

  me->plafond = (unsigned short)(me->plafond +
      lrULL("Valor a carregar", 0, canDeposit));

  printf("Novo saldo: %u creditos.\n", me->plafond);
}
