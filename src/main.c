/* main.c
 *
 * Tinha planos para fazer uma documentação de código mais
 * abrangente, mas estou a ficar sem tempo. Os ficheiros mais
 * bem documentados são os ficheiros de cabeçalho, especialmente
 * HashTable.h e SortedArray.h.
 */

#include "Str.h"
/* neste projeto utilizo dois tipos de estruturas
 * de dados principais: SortedArray (SA) e HashTable (HT).
 * Para mais detalhes de implementação dos mesmos sujiro
 * que verifique o seu código-fonte: */
#include "SortedArray.h"
/* a função seguinte é data como argumento ao
 * SA dos nomes dos alunos, para que este saiba
 * como comparar os seus elementos para pesquisar,
 * inserir, etc, ordenadamente */
static int namesSACompare(void *a, void *b)
{ return cmpStr((Str*)a, (Str*)b); }

#include "User.h"
/* a que se segue indica ao mesmo como aceder ao
 * campo "chave" que vai utilizar para fazer as
 * comparações. */
static void *namesSAAccess(void *element)
{ return (void*)(((User*)element)->name); }

#include "HashTable.h"
/* esta diz às HT (cujas chaves são numeros de estudante/funcionario)
 * como se obtêm as chaves. A razão pela qual não é preciso providenciar
 * uma função para comparações é que já existe uma pré-definida para
 * HT (ver HashTable.h) */
static void *idsGetKey(void *data)
{ return (void*)&((User*)data)->id; }

/* mostra a interface de login, declarada
 * abaixo de main, por motivos de legibilidade. */
Bool loginInterface(
    HT *studentIdsHT,
    HT *employeeIdsHT,
    User **getUser);

#include "Bool.h"
#include <stdio.h>
#include "io/console.h"
#include "io/studentMenu.h"
#include "io/employeeMenu.h"
#include "io/newUser.h"
#include "date.h"

#define FILENAME "data.bin"

int main(void) {
  User *me = NULL;
  SA *studentNamesSA;
  /* hashtables need to be initialized manually */
  HT *studentIdsHT = newNumHT(idsGetKey),
     *employeeIdsHT = newNumHT(idsGetKey);

  FILE *fp;

  if (!(fp = fopen(FILENAME, "rb"))) {
    printError("Nao foi possivel abrir o ficheiro '" FILENAME
        "'.\nA criar nova base de dados.\n");

    /* create a new sorted array and create the first user. */
    studentNamesSA = newSA(
        namesSAAccess,
        namesSACompare, 16);

    newUserInterface(NULL,
        studentIdsHT,
        employeeIdsHT,
        studentNamesSA);

  } else {
    /* load students ordered by name and insert
     * into ordered array and student id hashtable
     * (for quick binary search by name, and alphabetic lists,
     * and O(n) gets, removes, and inserts by id */
    studentNamesSA = loadSA(
        loadStudent,
        namesSAAccess,
        namesSACompare, fp);

    link(studentNamesSA, &studentIdsHT);
    /* load employee id hash table O(n) gets, removes and inserts */ 
    loadHT(&employeeIdsHT, loadEmployee, fp);

    fclose(fp);
  }

  initToday(); /* initialize today date object (shared) */

  do {
    Bool isStudent = loginInterface(
        studentIdsHT,
        employeeIdsHT, &me);

    if (me) {

      if (isStudent)
        studentMenu(me);
      else employeeMenu(
          me,
          studentIdsHT,
          employeeIdsHT,
          studentNamesSA);

    } else printError("Dados incorrectos.");
  } while(!ynmenu("\nSair"));


  fp = fopen(FILENAME, "wb");

  saveSA(studentNamesSA, saveStudent, fp);
  saveHT(employeeIdsHT, saveEmployee, fp);

  fclose(fp);

  return 0;
}

#include "Username.h"
Bool loginInterface(
    HT *studentIdsHT,
    HT *employeeIdsHT,
    User **getUser) {

  Username *un;
  Str *password;
  User *user;

  printTitle("LOGIN");
  un = usernameInterface("Login");
  password = lread("Password");

  if ((user = (User*) get(
          un->isStudent ? studentIdsHT : employeeIdsHT,
          (void*)&un->id))
      && !cmpStr(password, user->password)) {

    *getUser = user;

  } else *getUser = NULL;

  freeStr(password);
  return un->isStudent;
}
