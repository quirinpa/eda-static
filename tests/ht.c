#include "HashTable.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

static void *getKey(void *obj) {
	return obj;
}

#define inpSize 15
#define randstrSize 13
int main(void) {
	unsigned i = 0, j;
	HashTable *t = newStrHashTable(getKey);
	insert(&t, (void*)"hello");
	insert(&t, (void*)"world");

	for (i=0; i<inpSize; i++) {
		char *str = (char*) malloc(randstrSize*sizeof(char));
		srand(i+(unsigned)time(NULL));
		for (j = 0; j<randstrSize; j++)
			str[j] = (char)('a' + (unsigned char)(rand() % 26));
		str[j] = '\0';
		insert(&t, (void*)str);
	}
	/* printf("\nFinished inserting.\n"); */
	printf("%s %s\n",
			(char*)get(t, (void*)"hello"),
			(char*)get(t, (void*)"world"));

	return 0;
}
