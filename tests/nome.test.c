#include "stdio.h"
#include "nome.h"
#include "fileb.h"
#include "string.h"

#define nome "paulo ANDRE azevedo quIrino"
#define filename "nome.bin"

int main() {
	char *n = nome;
	char nsize = strlen(n);

	if (validNome(n, nsize)) {

		FILE *namef = fopen(filename, "w");
		bitCursor c = newBitCursor();
		saveNome(n, nsize, namef, &c);
		fcloseb(namef, &c);
		fclose(namef);

		char *loadedn;
		FILE *namefin = fopen(filename, "r");
		c = newBitCursor();
		char loadedsize = loadNome(&loadedn, namefin, &c);
		fclose(namefin); 

		printf("nome: \"%s\"\nsize: %d\n", loadedn, loadedsize);
		return 0;
	}

	return 1;
}
